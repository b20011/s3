class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    this.avg = undefined;
    this.willStudentPass = false;
    this.willPassWithHonor = false;

    if (grades.length === 4) {
      if (grades.every((grade) => grade >= 0 && grade <= 100)) {
        this.grades = grades;
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }
  }

  //Okay so to clarify that, if the method doesn't have a return value and you're planning on chaining the methods, you return "this" since "this" refers to the object caller?
  login() {
    console.log(`${this.email} has logged in.`);
    return this;
  }

  logout() {
    console.log(`${this.email} has logged out.`);
    return this;
  }

  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
    return this;
  }

  calcAvgGrade() {
    const calc = this.grades.reduce((prev, current) => prev + current);
    this.avg = calc / this.grades.length;
    return this;
  }

  willPass() {
    // console.log(this.calcAvgGrade().avg);
    this.willStudentPass = this.calcAvgGrade().avg >= 85 ? true : false;
    return this;
  }

  willPassWithHonors() {
    if (this.calcAvgGrade().avg < 85) {
      this.willPassWithHonor = undefined;
    }

    if (this.calcAvgGrade().avg >= 90) {
      this.willPassWithHonor = true;
    } else if (this.calcAvgGrade().avg >= 85 && this.calcAvgGrade().avg < 90) {
      this.willPassWithHonor = false;
    }

    return this;
  }
}

let studentOne = new Student('John', 'john@mail.com', [87, 89, 91, 93]);
let studentTwo = new Student('Jane', 'john@mail.com', [87, 89, 91, 93]);
let studentThree = new Student('Jessie', 'john@mail.com', [91, 89, 92, 93]);
let studentFour = new Student('Joe', 'john@mail.com', [78, 82, 79, 85]);

console.log(studentOne.login().willPass().willPassWithHonors());
// console.log(studentTwo);
// console.log(studentThree);
// console.log(studentFour);
